# Examples

  These examples condensate either facts we believe we need to document and share with other developers or technical pathways we are exploring. 
  The main tools in use are the JavaScript AJV validator and JSON schema itself. Both have websites with good documentation we will reference.
  
# List of Examples, ordered by complexity

* Most files also have a tiny comment at the top. It is worth reading.

## Web Site Tutorial

* File: `webSiteTutorial.js`

  This follows part of the [Getting Started Step-By-Step](https://json-schema.org/learn/getting-started-step-by-step.html) tutorial in the official web site.


## Static AJV Validator

* First File: `staticValidatorAJVTutorial.js`
* Second File: `usingStaticAJVValidator.js`

  This is a fairly basic example about how to create a **standalone** schema validator using AJV. The first file shows how to create it, the second how to call and apply it.
  

## Compare FarmOS.js and De Drupalized Schemas

* File: `compareFarmOSJSandDeDrupalized.js`

  Since **FarmOS.js** was already de drupalizing schemas, we tried them before generalizing our own functions. They work fine for our ends, except what seems to be a weird change in how `relationships` are restructured.
  The intention of this experiment was to check if the schemas that FarmOS.js provides are compliant enough to be used in AJV, which proved to be true at least up to a 90%.
  
  
## Using FarmOS Schemas

* File: `usingFarmOSSchemas.js`

 In this example we obtain a schema, test how the de drupalizing function can adapt a schema to allow it to be used with AJV and create some examples both accepted and rejected. It is worth of notice that the required fields are also covered by the schema and able to trigger validation errors.
 
## Using General FarmOS Validator

* File: `usingGeneralFarmOSValidator.js`

 Our CI/CD pipeline publishes a library containing validators for all type and bundles of entities, plus conventions. This example shows how to call and validate the library.
  
## Using Json Pointers

* File: `usingJSONPointers.js`

  This follows the chaper [structuring a complex schema](https://json-schema.org/understanding-json-schema/structuring.html?highlight=pointer) from the book **Understanding JSON Schema**.
  The main need for pointers was to ensure `relationship` fields work as expected.
  Pointers are references between different portions of the data, which allow to achieve things such as "the year in the timestamp date should also be mentioned in the name field". Meaning: they tie attributes of a *particular current datum* and not of the *general schema*.
  
## Define and Validate Convention

* File: `defineAndValidateConvention.js`

  In this file we do a first attempt at schematizing a `convention` and offering a **standalone validator** for it.
  

## Get Text From Convention

  This is a first attempt to read properly prepared "description" attributes from a convention, as well as the relationships, and get intelligible and descriptive enough documentation.

* File: `getTextFromConvention.js`.

  The example text is stored in this folder as `exampleDescriptionText.md`. It is currently not yet parsing hiearchy, everything else is already working, it is even nicely formatting.
