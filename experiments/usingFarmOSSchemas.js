// in this example we obtain a schema, test how the de drupalizing function can adapt a schema to allow it to be used with AJV and create some examples both accepted and rejected. It is worth of notice that the required fields are also covered by the schema and able to trigger validation errors.
const axios = require('axios');
const Ajv = require("ajv/dist/2019.js");
const addFormats = require('ajv-formats');
const { randomUUID } = require('crypto');
const {prepareDrupalSchemaForAJV} = require('../src/schema_utilities.js');

// let draft6Schema = JSON.parse(fs.readFileSync("./schemas/draft6.json"));

let landAssetSchemaDrupal = axios.get('https://ourscitest.farmos.net/api/asset/land/resource/schema')
  .then( res => res.data )
;

let landAssetSchema = landAssetSchemaDrupal
    .then( schema => prepareDrupalSchemaForAJV(schema) )
;

function buildValidator() {
    let validator = new Ajv({ allowUnionTypes: true, allErrors: true });
    addFormats(validator);

    // validator
    // draft6 schema is the JSON schema project official schema for a concise year/set of features (let's say 2019, this is not the newest one).
        // .addSchema(draft6Schema)
    ;
    return validator;
};

let landExample = {
    attributes: {
        name: "Example Land Asset",
        status: "active",
        land_type: "field"
    },
    relationships: {
        location: { data: [ {
            type: "asset--land",
            id: randomUUID()
        } ] }
    }
};

let landExample2 = {
    attributes:{
        name: "Example Land Asset",
        land_type: 2
    },
    relationships: {
        location: { data: [ {
            type: "asset--land",
        } ] }
    }
};

let validator = buildValidator();
let validate = landAssetSchema.then( schema => validator.compile(schema) );
validate.then( f => { return { is_valid: f(landExample), errors: f.errors }; } ).then(console.log).catch(console.error);
validate.then( f => { return { is_valid: f(landExample2), errors: f.errors }; } ).then(console.log).catch(console.error);

landAssetSchema.then( d => d.properties.relationships.properties.location.properties.data.items ).then( console.log );
