// We try to define a convention using JSON schema and test its validation.
const { randomUUID } = require('crypto');
const {prepareDrupalSchemaForAJV, buildValidator} = require('../src/schema_utilities.js');
const standaloneCode = require("ajv/dist/standalone").default;
const fs = require('fs');

let farmosSchemata = JSON.parse( fs.readFileSync(`${ __dirname }/../schemata/farmos.json`) );
let hyperSchema = JSON.parse( fs.readFileSync(`${ __dirname }/../schemata/hyper-schema.json`) );

let conventionSchema = {
    $id: "/conventions/log/activity/tillage",
    title: "Tillage Log Convention",
    type: 'object',
    properties: {
        activity_log: {
            type: 'object',
            description: "Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*. Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth. May have other taxonomy_term--log_category. Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)",
            $ref: farmosSchemata.log.activity.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        name: {
                            const: "tillage log"
                        }
                    }
                },
                relationships: {
                    type: 'object',
                    properties: {
                        quantity: {
                            type: "array",
                            minItems:2,
                            maxItems:2,
                            prefixItems: [
                                { type: "object",
                                    properties: {
                                        type: { const: "quantity--standard" },
                                        id: { const: { "$data": "/stir_quantity/id" } }
                                    }
                                },
                                { type: "object",
                                  properties: {
                                      type: { const: "quantity--standard" },
                                      id: { const: { "$data": "/depth_quantity/id" } }
                                  }
                                },
                            ],
                            // TODO
                            items: { type: "string" }
                        },
                        location: {
                            type: 'array',
                            minItems:1,
                        }
                    },
                    required: ['location']
                }
            },
            required: ['attributes', 'relationships']
        },
        stir_quantity: {
            type: 'object',
            description: "Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.",
            $ref: farmosSchemata.quantity.standard.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        label: { const: "stir" },
                        measure: { const: "ratio" }
                    }
                }
            }
        },
        depth_quantity: {
            type: 'object',
            description: "Must be labelled as *depth* and it's measure type is *length*.",
            $ref: farmosSchemata.quantity.standard.$id,
            properties: {
                label: { const:"depth" },
                measure: { const: 'length' },
            },
        }
    },
    required: ['activity_log']
};
exports.conventionSchema = conventionSchema;

fs.writeFileSync(`${__dirname}/../conventions/log--activity--tillage/schema.json`, JSON.stringify(conventionSchema, null, '  '));

let logUUID = randomUUID();
let stirUUID = randomUUID();
let depthUUID = randomUUID();

let example = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    activity_log: {
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: [
                {
                    type: "quantity--standard",
                    id: stirUUID
                },
                {
                    type: "quantity--standard",
                    id: depthUUID
                }
            ],
            location: [
                { type: "asset--land",
                  id: randomUUID()
                }
            ]
        },
        id: logUUID
    },
    stir_quantity: {
        attributes: {label: "stir"},
        id: stirUUID
    },
    depth_quantity: {
        attributes: {
            label:"depth"
        },
        id: depthUUID
    }
};
exports.validExample = example;

fs.writeFileSync(`${__dirname}/../conventions/log--activity--tillage/examples/correct/example_1.json`, JSON.stringify(example, null, '  '));


delete farmosSchemata.log.activity.$schema;
delete farmosSchemata.quantity.standard.$schema;

let validator = buildValidator({code: {source: true}});
validator.addSchema(farmosSchemata.log.activity);
validator.addSchema(farmosSchemata.quantity.standard);

let validate = validator.compile(conventionSchema);
// compile static validator module
let moduleCode = standaloneCode(validator, validate);
fs.writeFileSync( `${ __dirname }/../output/validators/validate-example-tillage-convention.js`, moduleCode);

// validate(example);
// validate.errors;


let exampleError = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    activity_log: {
        id: logUUID,
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: [ {
                type: "quantity--standard",
                id: '695e1d38-a1d6-4ed7-8d9d-c0b818a2cbfe'
            } ]
        },
        relationships: {
            quantity: [ {
                type: "quantity--standard",
                id: 'b71f7bb3-da43-40b1-8dc3-965e5f912763'
            } ]
        }
    },
    stir_quantity: {
        id: logUUID,
        attributes: {label: "stir"}
    },
};

// fs.writeFileSync(`${__dirname}/../conventions/log--activity--tillage/examples/incorrect/error_1.json`, JSON.stringify(example, null, '  '));


let noLocation = {};
Object.assign(noLocation, example);
delete noLocation.activity_log.relationships.location[0];
fs.writeFileSync(`${__dirname}/../conventions/log--activity--tillage/examples/incorrect/error_no_location.json`, JSON.stringify(example, null, '  '));

let idError = {};
Object.assign(idError, example);
idError.activity_log.relationships.quantity[0].id = randomUUID();
fs.writeFileSync(`${__dirname}/../conventions/log--activity--tillage/examples/incorrect/error_in_rel_id.json`, JSON.stringify(example, null, '  '));

let noQuantity = {};
Object.assign(noQuantity, example);
delete noQuantity.stir_rate;
delete noQuantity.activity_log.relationships.quantity;
fs.writeFileSync(`${__dirname}/../conventions/log--activity--tillage/examples/incorrect/error_missing_quantity.json`, JSON.stringify(example, null, '  '));

// validate(exampleError);
// validate.errors;
