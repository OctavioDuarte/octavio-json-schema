// The objective of this experiment is understanding how to use a JSON pointer to force two values to be equal.
// I will force the label in the quantity to be the name of the log. It does not make sense but its easy to locate.
const { randomUUID } = require('crypto');
const {prepareDrupalSchemaForAJV, buildValidator} = require('../src/schema_utilities.js');
const fs = require('fs');

let farmosSchemata = JSON.parse( fs.readFileSync("../schemata/farmos.json") );
let hyperSchema = JSON.parse( fs.readFileSync("../schemata/hyper-schema.json") );


// Create three entities of the type we will need in the convention, validate them independently, then try to validate them as part of the convention.


let conventionSchema = {
    $id: "/conventions/log/activity/tillage",
    title: "Tillage Log Convention",
    type: 'object',
    properties: {
        activity_log: {
            type: 'object',
            $ref: farmosSchemata.log.activity.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        name: {
                            const: "tillage log"
                        }
                    }
                },
            },
            required: ['attributes']
        },
        stir_quantity: {
            type: 'object',
            $ref: farmosSchemata.quantity.standard.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        label: {
                            const: { $data: "/activity_log/attributes/name" },
                                  },
                    }
                }
            }
        }
    },
    required: ['activity_log']
};

let logUUID = randomUUID();
let stirUUID = randomUUID();
let example = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants. 
    activity_log: {
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: [ {
                type: "quantity--standard",
                id: stirUUID
            } ]
        },
        id: randomUUID()
    },
    stir_quantity: {
        attributes: {label: "tillage log"},
        id: stirUUID
    }
};


delete farmosSchemata.log.activity.$schema;
delete farmosSchemata.quantity.standard.$schema;

let validator = buildValidator();
validator.addSchema(farmosSchemata.log.activity);
validator.addSchema(farmosSchemata.quantity.standard);
let validate = validator.compile(conventionSchema);

validate(example);
validate.errors;

let exampleError = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants. 
    activity_log: {
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: [ {
                type: "quantity--standard",
                id: stirUUID
            } ]
        },
        id: randomUUID()
    },
    stir_quantity: {
        attributes: {label: "stir"},
        id: stirUUID
    }
};

validate(exampleError);
validate.errors;
