const convention = require('../output/validators/validate-example-tillage-convention.js');
const { randomUUID } = require('crypto');
const { validExample } = require('../examples/defineAndValidateConvention.js');


test('Valid example is accepted.', () => {
    let validation = convention(validExample);
    console.log(convention.errors);

    expect(validation).toBe(true);
});

test('Missing location is rejected.', () => {
    let noLocation = {};
    Object.assign(noLocation, validExample);
    delete noLocation.activity_log.relationships.location[0];

    let validation = convention(noLocation);

    // convention.errors ? console.log(convention.errors): undefined;

    expect(validation).toBe(false);
});

test('Wrong relationship id is detected.', () => {
    let idError = {};
    Object.assign(idError, validExample);
    idError.activity_log.relationships.quantity[0].id = randomUUID();
    let validation = convention(idError);

    // convention.errors ? console.log(convention.errors): undefined;
    expect(validation).toBe(false);
});

test('Example lacking quantity is accepted, despite the prefixItems.', () => {
    let noQuantity = {};
    Object.assign(noQuantity, validExample);
    delete noQuantity.stir_rate;
    delete noQuantity.activity_log.relationships.quantity;

    let validation = convention(noQuantity);

    convention.errors ? console.log(convention.errors): undefined;

    expect(validation).toBe(true);
});
