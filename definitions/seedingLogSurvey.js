// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 11, planting_div_veg
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');

// ids for the examples
let seedingLogUUID = randomUUID();
let logCategoryUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let conventionUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let seedingRateUnitUUID = randomUUID();
let bedWidthUnitUUID = randomUUID();

// // Main entity: Seeding Log
let seedingLogExample = {
    id:seedingLogUUID,
    attributes: {
        name: "example div veg seeding log",
        status:"done",
        is_movement:"false"
    }
};

let seedingLogError = {
    id:seedingLogUUID,
    attributes: {
        name: "example div veg seeding log",
        status:"true",
        is_movement:"true"
    }
};


let seedingLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--seeding',
    name: 'seeding',
    validExamples: [seedingLogExample],
    erroredExamples: [seedingLogError]    
});

seedingLog.setMainDescription("A seeding or transplanting sets the location of the plant asset");

// required attribute is status
seedingLog.setConstant({
    attribute:"status",
    value:"done"
});

// required to set location of asset
seedingLog.setConstant({
    attribute:"is_movement",
    value:"false"
});

//log category examples
let logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "seeding"
    }
};
let logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        label: "activity"
    }
};

//taxonomy term - log_category
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "log_category",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});
logCategory.setMainDescription("The log category is seeding");

//// Quantities
//area examples
let areaPercentageExample = {
    id: areaPercentageUUID,
    attributes: {
        label: "area",
        measure: "area"
    }
};

//area overlay
let areaPercentage = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "area_percentage",
    validExamples: [areaPercentageExample]
    //erroredExamples: [areaPercentageError]
});
areaPercentage.setMainDescription("Area is the percentage of the field that the seeding applies to.");
areaPercentage.setConstant({
    attribute: "measure",
    value:"area"
});
areaPercentage.setConstant({
    attribute: "label",
    value:"area"
});

// unit example
let unitExample = {
    id: percentageUnitUUID,
    name: "%"
};
//taxonomy term - units
let unitPercentage = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "%"
});
unitPercentage.setMainDescription("Units for area are % of acres");

// seeding rate quantity
let seedingRateExample = {
    attributes: {
        measure:"rate",
        label: "seeding rate"
    }
};
let seedingRate = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "seeding_rate",
    validExamples: [ seedingRateExample ]
});
seedingRate.setMainDescription("The seeding rate indicates the mass of seed used for every surface unit.");
seedingRate.setConstant({
    attribute: "measure",
    value:"rate"
});
seedingRate.setConstant({
    attribute: "label",
    value:"seeding rate"
});

// seeding rate unit
// unit example
let rateUnitExample = {
    id: seedingRateUnitUUID,
    attributes: {
        name: "kg/hec"
    }
};
let rateUnitError = {
    id: seedingRateUnitUUID,
    attributes: {
        name: "kg/m^2"
    }
};
//taxonomy term - units
let rateUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "kg/hec",
    validExamples: [ rateUnitExample ],
    erroredExamples: [ rateUnitError ]
});
rateUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs/ac",
        "seeds/acre",
        "kg/hec",
        "seeds/hec",
        "plants per sq ft",
        "plants per sq m"
    ],
    description: "Several units are available, both imperial and metric. They are all compatible mass to area ratios."
});

//let seedingRateUnit = new builder.SchemaOverlay({
//    typeAndBundle: "taxonomy_term--unit",
//    name: "kg/hec",
//    validExamples: [ rateUnitExample ]
//});

// bed width
let bedWidthExample = {
    attributes: {
        measure:"length/depth",
        label: "bed width"
    }
};
let bedWidth = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "bed_width",
    validExamples: [ bedWidthExample ]
});
bedWidth.setMainDescription("The bed width indicates the size of permanent beds.");
bedWidth.setConstant({
    attribute: "measure",
    value:"length/depth"
});
bedWidth.setConstant({
    attribute: "label",
    value:"bed width"
});

// bed width unit
// unit example
let widthUnitExample = {
    id: bedWidthUnitUUID,
    attributes: {
        name: "feet"
    }
};
let widthUnitError = {
    id: bedWidthUnitUUID,
    attributes: {
        name: "kg"
    }
};
//taxonomy term - units
let widthUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "feet",
    validExamples: [ widthUnitExample ],
    erroredExamples: [ widthUnitError ]
});
widthUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "feet",
        "meters"
    ],
    description: "Several units are available, both imperial and metric."
});


// Convention

// Examples
let seedingConventionExample = {
   id: conventionUUID,
   seeding_log: {
    id: seedingLogUUID,
    attributes: {
        name: "example div veg seeding log",
        status: "done",
        is_movement: "false"
    },
    relationships: {
        category: [
            {
            type: "taxonomy_term--log_category",
            id:logCategoryUUID
            }   
        ],
        quantity: [
            {
            type: "quantity--standard",
            id: areaPercentageUUID
            }
        ]
    },
   },
   log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ]
        }
},
    area_unit: unitExample
};

let seedingConventionError = {
    id: conventionUUID,
    seeding_log: {
     id: seedingLogUUID,
     attributes: {
         name: "example div veg seeding log",
         status: "done",
         is_movement: false
     },
     relationships: {
         category: [
             {
             type: "taxonomy_term--log_category",
             id:logCategoryUUID
             }   
         ]
     },
    },
    log_category: logCategoryError,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ]
        }
    }
};

// Object
let seedingConvention = new builder.ConventionSchema({
    title: "Diverse Vegetable Seeding",
    version: "0.0.1",
    schemaName:"log--seeding--div_veg_planting",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:"Seeding log required for every planting asset.",
    validExamples: [seedingConventionExample],
    erroredExamples: [seedingConventionError]
});


seedingConvention.addAttribute( { schemaOverlayObject:seedingLog, attributeName: "seeding_log", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:areaPercentage, attributeName: "area_quantity", required: true } );
seedingConvention.addAttribute( { schemaOverlayObject:unitPercentage, attributeName: "area_unit", required: true } );


//do we connect a plant asset here? how?
seedingConvention.addRelationship( { containerEntity:"seeding_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
seedingConvention.addRelationship( { containerEntity:"seeding_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: true } );
seedingConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: true } );

let test = seedingConvention.testExamples();
let storageOperation = seedingConvention.store();
