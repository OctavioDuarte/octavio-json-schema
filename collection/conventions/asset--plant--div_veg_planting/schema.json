{
 "title": "Diverse Vegetable Planting",
 "type": "object",
 "$id": "www.gitlabrepo.com/version/farmos_conventions/0.0.1/conventions/asset--plant--div_veg_planting",
 "properties": {
  "plant_asset": {
   "title": "Plant asset",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "asset--plant"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255,
       "description": "The name of the asset."
      },
      "status": {
       "const": "active"
      },
      "archived": {
       "type": "string",
       "title": "Timestamp",
       "format": "date-time",
       "description": "The time the asset was archived."
      },
      "data": {
       "type": "string",
       "title": "Data"
      },
      "notes": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Notes"
      },
      "flag": {
       "type": "array",
       "title": "Flags",
       "description": "Add flags to enable better sorting and filtering of records.",
       "items": {
        "type": "string",
        "title": "Text value"
       }
      },
      "id_tag": {
       "type": "array",
       "title": "ID tags",
       "description": "List any identification tags that this asset has. Use the fields below to describe the type, location, and ID of each.",
       "items": {
        "type": "object",
        "properties": {
         "id": {
          "type": "string",
          "title": "ID of the tag"
         },
         "type": {
          "type": "string",
          "title": "Type of the tag"
         },
         "location": {
          "type": "string",
          "title": "Location of the tag"
         }
        }
       }
      },
      "geometry": {
       "const": null
      },
      "intrinsic_geometry": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Geometry"
        },
        "geo_type": {
         "type": "string",
         "title": "Geometry Type"
        },
        "lat": {
         "type": "number",
         "title": "Centroid Latitude"
        },
        "lon": {
         "type": "number",
         "title": "Centroid Longitude"
        },
        "left": {
         "type": "number",
         "title": "Left Bounding"
        },
        "top": {
         "type": "number",
         "title": "Top Bounding"
        },
        "right": {
         "type": "number",
         "title": "Right Bounding"
        },
        "bottom": {
         "type": "number",
         "title": "Bottom Bounding"
        },
        "geohash": {
         "type": "string",
         "title": "Geohash"
        },
        "latlon": {
         "type": "string",
         "title": "LatLong Pair"
        }
       },
       "title": "Intrinsic geometry",
       "description": "Add geometry data to this asset to describe its intrinsic location. This will only be used if the asset is fixed."
      },
      "is_location": {
       "type": "boolean",
       "title": "Is location",
       "description": "If this asset is a location, then other assets can be moved to it."
      },
      "is_fixed": {
       "type": "boolean",
       "title": "Is fixed",
       "description": "If this asset is fixed, then it can have an intrinsic geometry. If the asset will move around, then it is not fixed and geometry will be determined by movement logs."
      },
      "surveystack_id": {
       "type": "string",
       "title": "Surveystack ID",
       "maxLength": 255
      },
      "quick": {
       "type": "array",
       "title": "Quick form",
       "description": "References the quick form that was used to create this record.",
       "items": {
        "type": "string",
        "title": "Text value",
        "maxLength": 255
       }
      }
     },
     "required": [
      "name",
      "status"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "termination": {
       "title": "Terminated",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      },
      "file": {
       "type": "array",
       "title": "Files",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "image": {
       "type": "array",
       "title": "Images",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "location": {
       "type": "array",
       "title": "Current location",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "owner": {
       "type": "array",
       "title": "Owners",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "parent": {
       "type": "array",
       "title": "Parents",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "plant_type": {
       "type": "array",
       "minItems": 2,
       "maxItems": 2,
       "prefixItems": [
        {
         "type": "object",
         "properties": {
          "type": {
           "const": "taxonomy_term--plant_type"
          },
          "id": {
           "const": {
            "$data": "/species_taxonomy/id"
           }
          }
         }
        },
        {
         "type": "object",
         "properties": {
          "type": {
           "const": "taxonomy_term--plant_type"
          },
          "id": {
           "const": {
            "$data": "/variety_taxonomy/id"
           }
          }
         }
        }
       ],
       "items": {
        "type": "string"
       }
      },
      "season": {
       "type": "array",
       "minItems": 1,
       "maxItems": 1,
       "prefixItems": [
        {
         "type": "object",
         "properties": {
          "type": {
           "const": "taxonomy_term--season"
          },
          "id": {
           "const": {
            "$data": "/season_taxonomy/id"
           }
          }
         }
        }
       ],
       "items": {
        "type": "string"
       }
      }
     },
     "type": "object",
     "required": [
      "plant_type"
     ],
     "additionalProperties": false
    }
   },
   "description": "The planting is the main asset that all management logs will reference."
  },
  "species_taxonomy": {
   "title": "Plant type taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--plant_type"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "drupal_internal__tid": {
       "type": "integer",
       "title": "Term ID",
       "description": "The term ID."
      },
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      },
      "maturity_days": {
       "type": "integer",
       "title": "Days to maturity"
      },
      "transplant_days": {
       "type": "integer",
       "title": "Days to transplant"
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "title": "Vocabulary",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      },
      "parent": {
       "type": "array",
       "title": "Term Parents",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "companions": {
       "type": "array",
       "title": "Companions",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "crop_family": {
       "title": "Crop family",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      },
      "image": {
       "type": "array",
       "title": "Photos",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "description": "The species of the vegetable"
  },
  "variety_taxonomy": {
   "title": "Plant type taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--plant_type"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "drupal_internal__tid": {
       "type": "integer",
       "title": "Term ID",
       "description": "The term ID."
      },
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      },
      "maturity_days": {
       "type": "integer",
       "title": "Days to maturity"
      },
      "transplant_days": {
       "type": "integer",
       "title": "Days to transplant"
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "title": "Vocabulary",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      },
      "parent": {
       "type": "array",
       "title": "Term Parents",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "companions": {
       "type": "array",
       "title": "Companions",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      },
      "crop_family": {
       "title": "Crop family",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      },
      "image": {
       "type": "array",
       "title": "Photos",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "description": "The variety of the vegetable."
  },
  "season_taxonomy": {
   "title": "Season taxonomy term",
   "type": "object",
   "properties": {
    "id": {
     "type": "string",
     "format": "uuid"
    },
    "type": {
     "const": "taxonomy_term--season"
    },
    "meta": {
     "type": "object"
    },
    "attributes": {
     "description": "Entity attributes",
     "type": "object",
     "properties": {
      "drupal_internal__tid": {
       "type": "integer",
       "title": "Term ID",
       "description": "The term ID."
      },
      "status": {
       "type": "boolean",
       "title": "Published",
       "default": true
      },
      "name": {
       "type": "string",
       "title": "Name",
       "maxLength": 255
      },
      "description": {
       "type": "object",
       "properties": {
        "value": {
         "type": "string",
         "title": "Text"
        },
        "format": {
         "type": "string",
         "title": "Text format"
        }
       },
       "required": [
        "value"
       ],
       "title": "Description"
      },
      "weight": {
       "type": "integer",
       "title": "Weight",
       "description": "The weight of this term in relation to other terms.",
       "default": 0
      }
     },
     "required": [
      "name"
     ],
     "additionalProperties": false
    },
    "relationships": {
     "description": "Entity relationships",
     "properties": {
      "vid": {
       "title": "Vocabulary",
       "type": "object",
       "required": [
        "id",
        "type"
       ],
       "properties": {
        "id": {
         "type": "string",
         "title": "Resource ID",
         "format": "uuid",
         "maxLength": 128
        },
        "type": {
         "type": "string"
        }
       }
      },
      "parent": {
       "type": "array",
       "title": "Term Parents",
       "items": {
        "type": "object",
        "required": [
         "id",
         "type"
        ],
        "properties": {
         "id": {
          "type": "string",
          "title": "Resource ID",
          "format": "uuid",
          "maxLength": 128
         },
         "type": {
          "type": "string"
         }
        }
       }
      }
     },
     "type": "object",
     "additionalProperties": false
    }
   },
   "description": "A season taxonomy term should exist for each working period, and logs and assets should be associated to the related seasons."
  }
 },
 "description": "Main entity representing a cultivar which is going to be referenced by all of its logs.",
 "required": [
  "plant_asset",
  "species_taxonomy",
  "season_taxonomy"
 ]
}