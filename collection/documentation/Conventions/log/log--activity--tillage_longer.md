# Tillage Event, OurSci - v0.0.2

## tillage_log

* **Type: log--activity** 
* **Required: true** 
* Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*.
* Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth.
* May have other taxonomy_term--log_category.
* Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)

### stir_quantity

* **Type: quantity--standard** 
* **Required: false** 
* Must be labelled as *stir* and it's measure type is *ratio*.
* See documentation ADD LINK to get the standard specification for this ratio.

### depth_quantity

* **Type: quantity--standard** 
* **Required: false** 
* Must be labelled as *depth* and it's measure type is *length*.

#### depth_unit

* **Type: taxonomy_term--unit** 
* **Required: true** 
* Units are mandatory for any dimensional measurement, as big databases get unreliable when they are not explicitly mentioned, even if they seem evident.

### log_category

* **Type: taxonomy_term--log_category** 
* **Required: true** 
* The only identifier for a tillage log activity *MUST* be its tillage log category taxonomy term.

