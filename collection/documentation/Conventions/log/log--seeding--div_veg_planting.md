# Diverse Vegetable Seeding - v0.0.1

## seeding_log

* **Type: log--seeding** 
* **Required: true** 
* A seeding or transplanting sets the location of the plant asset

### log_category

* **Type: taxonomy_term--log_category** 
* **Required: true** 
* The log category is seeding

### area_quantity

* **Type: quantity--standard** 
* **Required: true** 
* Area is the percentage of the field that the seeding applies to.

#### area_unit

* **Type: taxonomy_term--unit** 
* **Required: true** 
* Units for area are % of acres

