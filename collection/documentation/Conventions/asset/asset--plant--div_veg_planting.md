# Diverse Vegetable Planting - v0.0.1

## plant_asset

* **Type: asset--plant** 
* **Required: true** 
* The planting is the main asset that all management logs will reference.

### species_taxonomy

* **Type: taxonomy_term--plant_type** 
* **Required: true** 
* The species of the vegetable

### variety_taxonomy

* **Type: taxonomy_term--plant_type** 
* **Required: false** 
* The variety of the vegetable.

### season_taxonomy

* **Type: taxonomy_term--season** 
* **Required: true** 
* A season taxonomy term should exist for each working period, and logs and assets should be associated to the related seasons.

