// This library provides tools that allow a user to easily design a convention following the format proposed by OurSci.
const fs = require('fs');
const {buildValidator, urlToStringPair} = require("./schema_utilities.js");


/**
 * Evaluates relationships in a convention in order to obtain a hierarchy used in building a tree.
 * @param {} root
 * @param {} convention
 * @returns {} 
 */
function getAttributes(root, convention) {
    let branches = convention
        .relationships
        .filter( rel => rel.containerEntity == root )
        .map( d => d.mentionedEntity )
    ;
    let output = {};
    branches.forEach( branch => output[branch] = {} );
    return output;
};

/**
 * Defines a set of modifications that fixate a base farmOS entity type--bundle for a specific use (an example would be obtaining a tillage log starting with log--activity).
 * Helper methods are offered to allow modification of attributes, which can either be set to constants or to enums, which should be accompained by a description to make the schema intelligible. A method to allow modification of the main description is also available. The object keeps track of modified attributes, to avoid overwriting of changes or contradictions.
 * It will  ask for examples of both valid and invalid entities, which will be used to test if the schema is really describing the kind of object the user intends to work with.
 * It will store an overlay which transforms the base schema into the intended one, as well as the examples, the merged final schema and will generate documentation based on the description fields.
 * It works with the fixed folder structure of the repo.
 * @param {string} typeAndBundle -- The entity--type pair, always two strings
 * @param {string} name -- Name used to identify this particular version of the schema.
 * @param {entity[]} validExamples -- List of valid objects that this class should include.
 * @param {entity[]} erroredExamples -- List of valid objects that this class should exclude.
 */
class SchemaOverlay {
    constructor({
        typeAndBundle,
        name,
        validExamples,
        erroredExamples,
        repoURL,
        storagePath=`${__dirname}/../collection/overlays`
    }) {
        this.name = name;
        this.typeAndBundle = typeAndBundle;
        this.validExamples = validExamples;
        this.erroredExamples = erroredExamples;
        this.repoURL = repoURL;
        this.schemaName = `${typeAndBundle}--${name}`;
        this.storagePath = storagePath;

        let validBaseSchemaPattern = typeAndBundle.match( /^[a-z_]*--[a-z_]*$/ ) ? typeAndBundle.match( /^[a-z_]*--[a-z_]*$/ ).index == 0 : false;
        if (!validBaseSchemaPattern) {
            throw new Error(`No schema for ${typeAndBundle} was found. This parameter should be a type--bundle pair, as farmOS mandates (examples are "log--lab_test" and "asset--land").`);
        };

        this.type = typeAndBundle.split("--")[0];
        this.bundle = typeAndBundle.split("--")[1];

        let baseSchema;

        try {
            baseSchema = JSON.parse( fs.readFileSync( `${__dirname}/../output/schemata/${this.type}/${this.bundle}/schema.json` ) );
            delete baseSchema.schema;
        } catch(e) {
            if (e.code == "ENOENT") {
                throw new Error(`No schema for ${typeAndBundle} was found. This might be due to the schema not existing in the farm, or the current "output/schemata" folder structure being out of date. You can update it using the "getAllSchemas.js" script, in the script folder.`);
            } else {
                throw e;
            }
            ;
        };

        this.baseSchema = baseSchema;
        this.unmodifiedAttributes = new Set( Object.keys( this.baseSchema.properties.attributes.properties ) );
        this.modifiedAttributes = new Set([]);
        this.overlay = {
            properties: {
                attributes: {
                    properties: {}
                }
            }
        };
        this.schema = baseSchema;
        let validatorObj = buildValidator();
        this.validate = validatorObj.compile(this.schema);
    };

    /**
     * User input only works over the overlay, the schema should always be the direct result of applying the overlay over the baseSchema.
     */
    updateSchema() {
        this.schema = {};
        this.schema.$id = `${this.repoURL}/schemata/${this.typeAndBundle.replace("--", "/")}/schema.json`;
        Object.assign(this.schema, this.baseSchema);
        Array.from( this.modifiedAttributes ).forEach( attr => {
            // Object.assign(this.schema.properties.attributes.properties[attr], this.overlay.properties.attributes.properties[attr]);
            this.schema.properties.attributes.properties[attr] = this.overlay.properties.attributes.properties[attr];
        } );
        this.schema.description = this.overlay.description;
        let validatorObj = buildValidator();
        this.validate = validatorObj.compile(this.schema);
    };

    /**
     * Ensures attribute definitions are not overwritten inside the same overlay.
     * @param {} attribute
     * @throws {} 
     */
    checkAttributeStatus(attribute) {
        if (!this.unmodifiedAttributes.has(attribute) && !this.modifiedAttributes.has(attribute)) {
            throw Error(`Attribute ${attribute} is unknown to the schema ${this.typeAndBundle}.`);
        } else if (!this.unmodifiedAttributes.has(attribute) && this.modifiedAttributes.has(attribute)) {
            throw Error(`Attribute ${attribute} has already been modified for this overlay, you are overwriting previously declared changes. Instead, make only one operation encompassing all the changes together.`);
        }
        ;
    };

    updateAttributeStatus(attribute) {
        this.unmodifiedAttributes.delete(attribute);
        this.modifiedAttributes.add(attribute);
    };

    /**
     * Will set the main, global description for the entity.
     * @param {} description
     */
    setMainDescription(description) {
        this.overlay.description = description;
        this.updateSchema();
    };

    /**
     * Sets an attribute as a constant as indicated by the given value.
     * @param {string} attribute -- Attribute name for the attribute which will be set to a constant.
     * @param {any} value -- The constant, which should be of the type allowed by the base schema for the attribute.
     */
    setConstant({ attribute, value, description }) {
        this.checkAttributeStatus(attribute);
        this.overlay.properties.attributes.properties[attribute] = { const: value };
        if (description) {
            this.overlay.properties.attributes.properties[attribute].description = description;
        };
        this.updateAttributeStatus(attribute);
        this.updateSchema();
    };

    /**
     * Sets an attribute as a constrained to a list of possible values.
     * @param {string} attribute -- Attribute name for the attribute which will be set to a constant.
     * @param {any[]} valuesArray -- The array of possible values, which should be of the type allowed by the base schema for the attribute.
     */
    setEnum({ attribute, valuesArray, description }) {
        this.checkAttributeStatus(attribute);
        this.overlay.properties.attributes.properties[attribute] = { enum: valuesArray };
        if (description) {
            this.overlay.properties.attributes.properties[attribute].description = description;
        };
        this.updateAttributeStatus(attribute);
        this.updateSchema();
    };

    /**
     * Build an AJV validator and ensure all valid examples are accepted and all error examples are rejected. Returns an array attribute for each set of examples, plus a general success attribute indicating wether all examples resulted as expected and a failedExamples array only listing entities for which there was no success (meanin unrejected error examples and rejected valid examples).
     */
    testExamples() {
        let generalOutput = {};

        generalOutput.validExamples = this.validExamples.map( example => {
            let output = {};
            output.valid = this.validate(example);
            output.success = output.valid;
            if (this.validate.errors) {
                output.errors = this.validate.errors;
            };
            output.entity = example;
            return output;
        } );
        generalOutput.erroredExamples = this.erroredExamples.map( example => {
            let output = {};
            output.valid = this.validate(example);
            output.success = !output.valid;
            if (this.validate.errors) {
                output.errors = this.validate.errors;
            };
            output.entity = example;
            return output;
        } );

        generalOutput.success  = [ ... generalOutput.validExamples, ... generalOutput.erroredExamples ]
            .map( ex => ex.success )
            .every( bool => bool )
        ;
        generalOutput.failedExamples = [ ... generalOutput.validExamples, ... generalOutput.erroredExamples ]
            .filter( ex => !ex.success )
        ;
        return generalOutput;
    };

    /**
     * Will test the schema against the example and, if succesful, store both the schema and examples in adequate folders. The option to omit testing, (`test:false`) is there because in some occasions when building a `ConventionSchema` a user might not provide examples for the individual overlays, but for the general data structure and in that case the validation will happen at the `Convention` level. It should not be used otherwise, it's better to have examples and testing for each schema.
     * @param {boolean} test -- Wether to test against the examples. It's highly adviced to always provide examples and test.
     * @returns {Object} output -- Storage operation results, including path and test results. 
     * @throws {Error} -- Informs if examples are missing when testing is attempted. 
     */
    store(test=true) {
        let targetPath = `${this.storagePath}/${this.schemaName}`;
        fs.mkdirSync( targetPath, { recursive: true }, console.error );
        fs.mkdirSync( `${ targetPath }/examples/correct`, { recursive: true }, console.error );
        fs.mkdirSync( `${ targetPath }/examples/incorrect`, { recursive: true }, console.error );
        let valid = "not tested";
        if (test) {
            let test = this.testExamples();
            valid = test.success;
            if (!valid) {
                throw new Error(`The 'testExamples' method failed. The overlay either lacks examples, or is rejecting a valid example or accepting an invalid example.`);
            };
            this.validExamples.forEach( (example,i) => {
                fs.writeFileSync( `${ targetPath }/examples/correct/example_${i+1}.json`, JSON.stringify(example, null," "), console.error );
            } );
            this.erroredExamples.forEach( (example,i) => {
                fs.writeFileSync( `${ targetPath }/examples/incorrect/error_${i+1}.json`, JSON.stringify(example, null," "), console.error );
            } );
        };

        fs.mkdirSync( targetPath, { recursive: true }, console.error );
        fs.mkdirSync( `${ targetPath }/examples/correct`, { recursive: true }, console.error );
        fs.mkdirSync( `${ targetPath }/examples/incorrect`, { recursive: true }, console.error );

        fs.writeFileSync( `${ targetPath }/schema.json`, JSON.stringify(this.schema, null," "), console.error );

        let output = {
            path: targetPath,
            test: test,
            valid: valid
        };

        fs.writeFileSync( `${ targetPath }/object.json`, JSON.stringify(this), console.error );

        return output;
    };
};

/**
 * A 'convention' is a schema coordinating several 'first level' entities (as schematized by farmOS), encoding every aspect of a complex data source in a discoverable, well documented and easy to interpret structure. This tool helps build such a schema. It fixates a list of possible 'first level entities', with meaningful attribute names inside the convention, as well as their contents and relationships.
 * A 'convention' will typically consist of several entities, ideally fine tuned via an overlay to better refine the data structure.
 * Description attributes will allow the convention to be self documenting.
 * @param {string} title -- Title for the schema.
 * @param {semver} version -- Semver.
 * @param {string} description -- Mildly detailed text explaining the convention.
 * @param {entity[]} validExamples -- List of valid objects that this class should include.
 * @param {entity[]} erroredExamples -- List of valid objects that this class should exclude.
 */
class ConventionSchema {
    constructor({
        title,
        schemaName,
        version,
        repoURL,
        description,
        validExamples,
        erroredExamples,
        storagePath=`${__dirname}/../collection`
    }) {
        let schemaNameCompliant = schemaName.match( /[a-z_]*--[a-z_]*--[a-z_]*/ );
        if (!schemaNameCompliant) {
            throw new Error(`Parameter schemaName does not follow the sugested structure, which is adding a descriptive noun to the type--bundle pair of the main entity. For example, a convention centered around an activity log for tillage shoudl be called 'log--activity--tillage'`);
        };
        this.title = title;
        this.description = description;
        this.schemaName = schemaName;
        this.version = version;
        this.repoURL = repoURL,
        this.validExamples = validExamples;
        this.erroredExamples = erroredExamples;
        this.overlays = {};
        this.relationships = [];
        this.required = [];
        this.storagePath = storagePath;
    };

    /**
     * Generates the final schema according to the descriptors stored in the object. Particularly writes attributes and relationships.
     * @returns {} 
     */
    updateSchema() {
        this.schema = {
            title: this.title,
            type: 'object',
            $id: `${this.repoURL}/${this.version}/conventions/${this.schemaName}`,
            properties: {},
            description: this.description,
            required: this.required,
        };
        Object.keys(this.overlays).forEach( attributeName => {
            this.schema.properties[attributeName] = this.overlays[attributeName].schema;
            delete this.schema.properties[attributeName].$id;

            //  add all relationships belonging to this entity
            let relations = this.relationships
                .filter( relObj => relObj.containerEntity == attributeName  )
            ;
            let relationFields = new Set( relations.map( d => d.relationName ) );

            relationFields.forEach( fieldName => {
                let objects = relations.filter( d => d.relationName == fieldName );
                if (!this.schema.properties[attributeName].properties.relationships) {
                    this.schema.properties[attributeName].properties.relationships = {
                        properties: {}
                    };
                };
                this.schema.properties[attributeName].properties.relationships.properties[fieldName] = {
                    type: "array",
                    minItems:objects.length,
                    maxItems:objects.length,
                    prefixItems: relations.filter( rel =>  rel.relationName == fieldName )
                        .map( relObj => {
                            let output = {
                                type: "object",
                                properties: {
                                    type: { const: this.overlays[relObj.mentionedEntity].typeAndBundle },
                                    id: { const: { $data: `/${relObj.mentionedEntity}/id` } }
                                }
                            };
                            return output;
                        } ),
                    items: { type: "string" }
                };
            } );
        } );
        const validatorObj = buildValidator();
        this.validate = validatorObj.compile(this.schema);
    };

    /**
     * Adds an attribute which should be an entity represented as a schema overlay object.
     * @param {SchemaOverlay|String} schemaOverlayObject -- An object of the SchemaOverlay class or a string. If it is a string, it should be the "type--bundle" class of the desired base FarmOS schema or a preexisting overlay. 
     * @param {string} attributeName -- Name identifying the entity inside the schema. Typically it is not the type--bundle pair but a unique name related to its functionality inside the schema. An example would be the case in which a convention involves many quantities: you can't name each quantity "quantity", instead the logical naming pattern is alluding to what's being measured.
     * @param {boolean} required -- Indicate wether the attribute should be marked as 'required' by the schema, in which case every submission lacking this attribute will be marked as rejected.
     */
    addAttribute( { schemaOverlayObject, attributeName, required } ) {
        // If the argument is a string, it might name either a schema overlay or a base schema. Determine which one it is and load the object
        if ( typeof schemaOverlayObject == 'string' ) {
            let isOverlay = fs.readdirSync(`${this.storagePath}/overlays`).find( folder => folder == schemaOverlayObject );
            if (isOverlay) {
                schemaOverlayObject = JSON.parse( fs.readFileSync(`${this.storagePath}/overlays/${schemaOverlayObject}/object.json`) );
            } else {
                schemaOverlayObject = new SchemaOverlay({
                    typeAndBundle: schemaOverlayObject,
                    name: schemaOverlayObject,
                });
            }
            ;
        };
        this.overlays[attributeName] = schemaOverlayObject;

        if ( required ) {
            this.required.push(attributeName);
        };

        this.updateSchema();
};

    /**
     * Writes relationships between entities. Relationships in JSON schema are always mentioned in the topmost entity (named 'containerEntity') inside its 'relationships' first level property, which should have a suitably named field. To allow us to enforce the contents of these fields, we need to constrain them more than what FarmOS standard actually asks for, using arrays with a fixed order and number of items. You can add many relationships for the same field by succesively using this method, the schema compiler will correctly build the array.
     * @param {string} containerEntity -- Attribute name (as given when using 'addAttribute') for the entity hosting the relationship.
     * @param {string} relationName -- Field in 'relationships' under which the relationship will be written.
     * @param {string} mentionedEntity -- Attribute name (as given when using 'addAttribute') for the entity mentioned by the relationship.
     * @param {boolean} required -- Wether to rejec every submission missing this particular relationship.
     */
    addRelationship( { containerEntity, relationName, mentionedEntity, required } ) {
        // Check if the relationship field is valid.
        if (!this.overlays[containerEntity]) {
            throw new Error(`Container entity '${containerEntity}' has not been added as an attribute, please add a SchemaOverlay describing it before using it in a relationship.`);
        };
        if (!this.overlays[ mentionedEntity ]) {
            throw new Error(`Mentioned entity '${mentionedEntity}' has not been added as an attribute, please add a SchemaOverlay describing it before using it in a relationship.`);
        };
        let containerSchema = this.overlays[containerEntity].schema;
        let relationshipAttributes = Object.keys( containerSchema.properties.relationships.properties );

        if (!relationshipAttributes.includes(relationName)) {
            throw new Error(`The provided relationship field, '${relationName}', doesn't seem to be a valid field in the schema for '${containerSchema.properties.type.const}'. Available fields are: ${relationshipAttributes.join(", ")}.`);
        };

        this.relationships.push( { containerEntity:containerEntity, relationName: relationName, mentionedEntity: mentionedEntity } );
        this.updateSchema();
    };

    /**
     * Organizes all attributes of a convention into a hierarchical binary tree, under the assumption the tree is binary.
     * @param {ConventionSchema} -- convention
     * @returns {tree} -- An object in which the attributes form the structure of the tree, such as { node: { branch: { leaf: {} }, branch2: {leaf2:{}, leaf3:{}} } }. 
     */
    getAttributesTree() {
        let allAttributes = Object.keys( this.overlays );
        let tree = {};
        allAttributes
            .filter( attr => !this.relationships.find( rel => rel.mentionedEntity == attr ) )
            .forEach( attribute => {
                tree[attribute] = {};
                let rels = getAttributes(attribute, this);
                Object.keys( rels ).forEach( rel =>
                    tree[attribute][rel] = getAttributes(rel, this)
                );
            });
        return tree;
    };

    /**
    * Utility for the documentation of conventions, turns an attributes tree into an organized, hierarchical mardkwon file.
    * @param {integer} currentLevelIndex -- Used to control the level of recursion, should start as 0.
    * @param {string} currentBranch -- Contents of the attribute currently under evaluation.
    * @param {string} currentAttributeName -- Name of the attribute currently under evaluation.
    * @param {string} text -- Variable in which the mardkwon is acumulated.
    * @returns {} 
    */
    printTreeBranch( { currentLevelIndex=0, currentBranch, currentAttributeName, text = "" } ) {
        if (!text) {
            text = "# ".concat( this.title," - v", this.version, "\n\n" );
        };
        let entity = this.schema.properties[currentAttributeName];
        let overlay = this.overlays[currentAttributeName];
        if (overlay && entity) {
            text = text.concat(
                `${"#".repeat(currentLevelIndex)} ${currentAttributeName}`,
                "\n\n",
                `* **Type: ${urlToStringPair( overlay.typeAndBundle )}** \n`,
                `* **Required: ${this.required.includes(currentAttributeName)}** \n`,
                `* ${entity.description.replace( /\.\s/g, `.\n* ` )}`, "\n\n"
            );
        };
        if (Object.keys(currentBranch).length > 0) {
            Object.keys(currentBranch).forEach( key => { text = this.printTreeBranch({ currentLevelIndex: currentLevelIndex+1, currentBranch:currentBranch[key], currentAttributeName:key, text:text }); } );
        }
        return text;
    };

    /**
     * Generate documentation about this convention, relying on the 'description' fields and the hierarchical structure.
     */
    document() {
        let tree = this.getAttributesTree();
        let text = this.printTreeBranch( { currentLevelIndex: 1, currentBranch: tree, curentAttributeName: "" } );
        return text;
    };

    /**
     * Build an AJV validator and ensure all valid examples are accepted and all error examples are rejected. Returns an array attribute for each set of examples, plus a general success attribute indicating wether all examples resulted as expected and a failedExamples array only listing entities for which there was no success (meanin unrejected error examples and rejected valid examples).
     */
    testExamples() {
        let generalOutput = {};

        if ( !this.validExamples || !this.erroredExamples ) {
            throw new Error(`Testing can't happen because examples are missing either in the valid or errored attribute`);
        };

        if ( this.validExamples.length == 0 || this.erroredExamples.length == 0 ) {
            throw new Error(`Testing can't happen because examples are missing either in the valid or errored attribute`);
        };

        generalOutput.validExamples = this.validExamples.map( example => {
            let output = {};
            output.valid = this.validate(example);
            output.success = output.valid;
            if (this.validate.errors) {
                output.errors = this.validate.errors;
            };
            output.entity = example;
            return output;
        } );
        generalOutput.erroredExamples = this.erroredExamples.map( example => {
            let output = {};
            output.valid = this.validate(example);
            output.success = !output.valid;
            if (this.validate.errors) {
                output.errors = this.validate.errors;
            };
            output.entity = example;
            return output;
        } );

        generalOutput.success  = [ ... generalOutput.validExamples, ... generalOutput.erroredExamples ]
            .map( ex => ex.success )
            .every( bool => bool )
        ;
        generalOutput.failedExamples = [ ... generalOutput.validExamples, ... generalOutput.erroredExamples ]
            .filter( ex => !ex.success )
        ;
        return generalOutput;
    };

    /**
     * Tests the schema against the provided examples and, if succesful, stores them in the adequate data structure to be incorporated into the json schema publication.
     * @returns {} 
     * @throws {} 
     */
    store() {
        this.updateSchema();
        let test = this.testExamples();
        let valid = test.success;
        if (!valid) {
            throw new Error(`The 'testExamples' method failed. The schema either lacks examples, or is rejecting a valid example or accepting an invalid example.`);
        };

        Object.keys( this.overlays ).forEach( overlayName => this.overlays[overlayName].store(false) );

        let targetPath = `${this.storagePath}/conventions/${this.schemaName}`;
        let mainEntityType = this.schemaName.split("--")[0];
        let documentationFolder = `${this.storagePath}/documentation/Conventions/${ mainEntityType }`;
        let documentationPath = `${ documentationFolder }/${this.schemaName}.md`;
        fs.mkdirSync( targetPath, { recursive: true }, console.error );
        fs.mkdirSync( `${ targetPath }/examples/correct`, { recursive: true }, console.error );
        fs.mkdirSync( `${ targetPath }/examples/incorrect`, { recursive: true }, console.error );

        fs.writeFileSync( `${ targetPath }/schema.json`, JSON.stringify(this.schema, null," "), console.error );
        this.validExamples.forEach( (example,i) => {
            fs.writeFileSync( `${ targetPath }/examples/correct/example_${i+1}.json`, JSON.stringify(example, null," "), console.error );
        } );
        this.erroredExamples.forEach( (example,i) => {
            fs.writeFileSync( `${ targetPath }/examples/incorrect/error_${i+1}.json`, JSON.stringify(example, null," "), console.error );
        } );

        let output = {
            path: targetPath,
            valid:valid,
            test: test
        };

        fs.writeFileSync( `${ targetPath }/object.json`, JSON.stringify(this), console.error );

        try {
            let documentation = this.document();
            fs.mkdirSync( documentationFolder, {recursive:true} );
            fs.writeFileSync( documentationPath, documentation, console.error );
        } catch(error) {
            console.log("Documentation couldn't be generated, probably due to the lack of description in one of the attribute overlays.");
            console.log(error);
        };

        return output;
    };
};

exports.SchemaOverlay = SchemaOverlay;
exports.ConventionSchema = ConventionSchema;
