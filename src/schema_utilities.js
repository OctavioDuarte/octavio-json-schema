// Tools needed to work with FarmOS drupal native schemas using AJV
const Ajv = require("ajv/dist/2020.js");
const addFormats = require('ajv-formats');
const standaloneCode = require("ajv/dist/standalone").default;
const fs = require('fs');

// NOTE replaced with FarmOS.js
// function prepareDrupalSchemaForAJV(drupalSchema) {
//     // clone the schema so the original doesn't get changed
//     let schema = {};
//     // The relevant schema is not the hyper schema (used by the API) but the general json schema schema
//     // console.log(schema);
//     // if (schema.$schema.includes("hyper")) {
//     //     schema.$schema = "http://json-schema.org/draft-06/schema#";
//     // };

//     schema.properties = drupalSchema.definitions;
//     schema.type = "object";
//     schema.$id = drupalSchema.$id;
//     schema.title = drupalSchema.title;
//     schema.additionalProperties = false;

//     let relationshipKeys = Object.keys(schema.properties.relationships.properties);
//     relationshipKeys.forEach( key => {
//         delete schema.properties.relationships.properties[key].links;
//     } );

//     return schema;
// };

/**
 * Since FarmOS.js returns a monolithic object (which we call `schemataTree` in this documentation), we will store discrete schemas in a tree-like structure of folders, witha  depth level for type and another one for bundle.
 * @param {schemataTree} schemataTree --  De Drupalized Schemas arranged in a tree branched by hierarchy, as FarmOS.js returns.
 */
function storeDeDrupalizedSchemas(schemataTree) {

    fs.mkdirSync( "output/schemata", { recursive: true }, console.error );
    Object.keys(schemataTree).forEach( typeKey => {
        Object.keys(schemataTree[typeKey]).forEach( bundleKey => {
            let schema = schemataTree[typeKey][bundleKey];
            delete schema.$schema;
            fs.mkdirSync( `${__dirname}/../output/schemata/${typeKey}/${bundleKey}/`, { recursive: true }, console.error );
            let path = `${__dirname}/../output/schemata/${typeKey}/${bundleKey}/schema.json`;
            fs.writeFileSync(path, JSON.stringify( schema, null," " ));
            console.log(`writing ${typeKey}--${bundleKey}`);
        } );
    } );
};


/**
 * Builds an AJV object, already parametrized as FarmOS schemas require (this is: it effectively deals with the biolerplate).
 * @param {Object} extraAJVArguments -- Allows to directly send parameters to the new Ajv command. We typically use it to activate the `source` attribute, required to compile static validators.
 * @returns {Ajv} 
 */
function buildValidator(extraAJVArguments = false) {
    let ajvArguments = {
        // don't know if needed, test
        allowUnionTypes: true,
        // needed to get all the errors at once, we do not only need to reject a schema but also provide enought information to fix the entity
        allErrors: true,
        // needed to use internal data constraints in conventions, currently to validate relationships.
        $data:true,
        // needed to compile static validators
        // TODO remove, moved to thenew extraAJVArguments when needed
        // code: {source: true}
    };
    if (extraAJVArguments) {
        Object.keys(extraAJVArguments).forEach( key => { ajvArguments[key] = extraAJVArguments[key]; } );
    }
    let validator = new Ajv(ajvArguments);
    // TODO
    // apparently, the only format we need is date-time, so this function should be replaced with the schema
    addFormats(validator);

    // validator
    // draft6 schema is the JSON schema project official schema for a concise year/set of features (let's say 2019, this is not the newest one).
    // .addSchema(draft6Schema)
    ;
    return validator;
};


/**
 * Provided with a type/bundle pair and a source of schemata in which both exist, it will initialize an Ajv object, create a standalone/static validator and store it in the adequate directory (with one level for type, another for bundle).
 * @param {string} type -- A FarmOS type known to the `schemataTree` provided.
 * @param {string} bundle -- A FarmOS bundle known to the `schemataTree` provided.
 * @param {schemataTree} schemataTree -- De Drupalized Schemas arranged in a tree branched by hierarchy, as FarmOS.js returns.
 * @returns {} 
 */
function buildStaticValidator(type, bundle, schemataTree) {
    let validatorObj = buildValidator( { code: { source: true } } );
    let schema = schemataTree[type][bundle];
    delete schema.$schema;
    let validator = validatorObj.compile(schema);
    let moduleCode = standaloneCode(validatorObj, validator);

    // Now you can write the module code to file
    let path = `${ __dirname }/../output/validators/${bundle}_${type}.js`;
    fs.writeFileSync(path, moduleCode);
    return path;
};

function compileConventionsValidator() {
    let schemasArgument = {
        schemas:[],
        code: {
            source: true
        }
    };
    let mapping = {};
    let conventions = fs.readdirSync( `${__dirname}/../collection/conventions`, (error, files) => {
        return files;
    } );
    // add all schemas into the object we need
    conventions.forEach( conventionName => {
        console.log(`Working on ${ conventionName }`);
        let schema = JSON.parse( fs.readFileSync( `${__dirname}/../collection/conventions/${conventionName}/schema.json` ) );
            schemasArgument.schemas.push(schema);
            mapping[conventionName] = schema.$id;
    } );
    let validatorObj = buildValidator(extraAJVArguments=schemasArgument);
    let moduleCode = standaloneCode(validatorObj, mapping);

    // Write the module code to file
    let path = `${ __dirname }/../output/validators/allConventions.js`;
    fs.writeFileSync(path, moduleCode);
};


/**
 * Will create a standalone validator that integrates all the type/bundle schemas mentioned in the schemata tree provided. It will be stored in the validators folder.
 * @param {schemataTree} schemataTree -- De Drupalized Schemas arranged in a tree branched by hierarchy, as FarmOS.js returns.
 */
function compileGeneralValidator(schemataTree) {
    let schemasArgument = {
        schemas:[],
        code: {
            source: true
        }
    };
    let mapping = {};
    // add all schemas into the object we need
    Object.keys(schemataTree).forEach( typeKey => {
        Object.keys(schemataTree[typeKey]).forEach( bundleKey => {
            let schema = schemataTree[typeKey][bundleKey];
            delete schema.$schema;
            schemasArgument.schemas.push(schema);
            mapping[`${typeKey}__${bundleKey}`] = schema.$id;
        } );
    } );
    let validatorObj = buildValidator(extraAJVArguments=schemasArgument);
    let moduleCode = standaloneCode(validatorObj, mapping);

    // Write the module code to file
    let path = `${ __dirname }/../output/validators/allFarmOSSchemas.js`;
    fs.writeFileSync(path, moduleCode);
};


function urlToStringPair( url ) {
    return url
        .replace( /(.*api\/)|(\/resource\/schema.*)/g, "" )
        .replace( "\/", "--" )
    ;
};

// exports.prepareDrupalSchemaForAJV = prepareDrupalSchemaForAJV;
exports.buildValidator = buildValidator;
exports.buildStaticValidator = buildStaticValidator;
exports.compileGeneralValidator = compileGeneralValidator;
exports.compileConventionsValidator = compileConventionsValidator;
exports.storeDeDrupalizedSchemas = storeDeDrupalizedSchemas;
exports.urlToStringPair = urlToStringPair;
